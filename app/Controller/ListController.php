<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP ListController
 * @author nalu
 */
class ListController extends AppController {
    public $uses = ['User', 'Manager', 'Movie', 'Kizuki', 'Map'];
    public function index() {
        $user = $this->Session->read("Auth.User");
        $movies = $this->Movie->find('all', 
                ['fields' => ['id', 'title', 'created'],
                'order' => ['created' => 'asc'],
                'recursive' => -1]
        );
        $movieList = array();
        for ($i=0; $i<count($movies); $i++) {
            $maxCount = $this->Kizuki->find('first',
                    ['fields'=>['max(count) as count_max'],
                    'conditions'=>[
                    'movie_id'=>$movies[$i]['Movie']['id'],
                    'user_id'=>$user['id']]]
            );
            $cnt = 0;
            if (!empty($maxCount[0]['count_max'])) {
                $cnt = $maxCount[0]['count_max'];
            }

            $movieList[] = ['id' => $movies[$i]['Movie']['id'],
                'title' => $movies[$i]['Movie']['title'],
                'created' => $movies[$i]['Movie']['created'],
                'kizukiCount' => $cnt
                ];
        }
        $this->set('movieList', $movieList);
        $this->set('title_for_layout','Video list - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-yellow layout-top-nav');                
    }

    public function manager_index() {
        $movies = $this->Movie->find('all', 
                ['fields' => ['id', 'type', 'title', 'created'],
                'order' => ['created' => 'asc'],
                'recursive' => -1]
        );
        $movieList = array();
        for ($i=0; $i<count($movies); $i++) {
            $kcount = $this->Kizuki->find('count', 
                    ['conditions' => ['movie_id' => $movies[$i]['Movie']['id']]]
            );
            $movieList[] = ['id' => $movies[$i]['Movie']['id'],
                'type' => $movies[$i]['Movie']['type'],
                'title' => $movies[$i]['Movie']['title'],
                'created' => $movies[$i]['Movie']['created'],
                'kizukiCount' => $kcount
                ];
        }
        $this->set('movieList', $movieList);
        $this->set('title_for_layout','Video list - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-green layout-top-nav');                
    }
    
    public function manager_add() {
        if($this->request->is('POST')) {
            $movie = [
                'type' => $this->data['Movie']['type'],
                'title' => $this->data['Movie']['title'],
                'movie_path' => $this->data['Movie']['movie_path']
            ];
            $this->Movie->create();
            $this->Movie->save($movie);
            $movie_id = $this->Movie->getLastInsertID();
            if ($this->data['Movie']['type'] == 'MP') {
                if (isset($this->data['Movie']['csvfile'])) {
                    if (is_uploaded_file($this->data['Movie']['csvfile']['tmp_name'])) {
                        $ext = substr($this->data['Movie']['csvfile']['name'], strrpos($this->data['Movie']['csvfile']['name'], '.') + 1);
                        if ($ext === 'csv') {
                            $this->Map->loadCSV($movie_id, $this->data['Movie']['csvfile']['tmp_name']);
                        }
                    }
                }
            }
            $this->redirect(['controller'=>'List', 'action'=>'index']);
        }

        
        $this->set('types', ['TL' => 'タイムライン', 'MP' => 'マップ']);
        $this->set('title_for_layout','Video - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-green layout-top-nav');                
    }

    public function manager_edit($id=null) {
        if($this->request->is('put')) {
            $movie = [
                'id' => $this->data['Movie']['id'],
                'type' => $this->data['Movie']['type'],
                'title' => $this->data['Movie']['title'],
                'movie_path' => $this->data['Movie']['movie_path']
            ];
            $this->Movie->save($movie);
            $movie_id = $this->Movie->getLastInsertID();
            if ($this->data['Movie']['type'] == 'MP') {
                if (isset($this->data['Movie']['csvfile'])) {
                    if (is_uploaded_file($this->data['Movie']['csvfile']['tmp_name'])) {
                        $ext = substr($this->data['Movie']['csvfile']['name'], strrpos($this->data['Movie']['csvfile']['name'], '.') + 1);
                        if ($ext === 'csv') {
                            $this->Map->loadCSV($movie_id, $this->data['Movie']['csvfile']['tmp_name']);
                        }
                    }
                }
            }
            $this->redirect(['controller'=>'List', 'action'=>'index']);
        }

        $movieData = $this->Movie->find('first', [
            'conditions' => ['id' => $id],
            'recursive' => -1
        ]);
        $this->data = $movieData;
        $this->set('types', ['TL' => 'タイムライン', 'MP' => 'マップ']);
        $this->set('title_for_layout','Video Edit - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-green layout-top-nav');                
    }
    
    public function manager_delete($id=null) {
        $this->Movie->delete($id);
        
        $this->redirect(['controller'=>'List', 'action'=>'index']);
    }
}

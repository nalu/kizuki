<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP UsersController
 * @author nalu
 */
class UsersController extends AppController {
    public $uses = ['User', 'Manager'];
    public function index() {
        $this->redirect(['controller'=>'List', 'action'=>'index']);
    }
    
    public function login() {
        if ($this->request->is('post')) {
			if($this->Auth->login()) {
				$this->redirect(['controller'=>$this->Auth->loginRedirect['controller'], 
                                 'action'=>$this->Auth->loginRedirect['action']]);
            }else{
                $this->Session->setFlash('メールアドレスかパスワードが正しくありません!', 'default', ['class' => 'text-red']);
            }
        }        
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition login-page');
    }
    
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function regist() {
        if($this->request->is('ajax')) {
            $this->autoRender = false;
            $user = $this->data['User'];
            unset($user['password2']);
            $this->User->create();
            $this->User->save($user);
            $status = true;
            $result = null;
            return json_encode(compact('status', 'result'));
        }
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition register-page');
    }    
    
    public function reg_validation() {
        $this->autoRender = false;
        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
        $cnt = $this->User->find('count', array(
            'conditions' => ['account' => $this->data['User']['account']],
        ));
        $status = true;
        $result = ['account' => $cnt];
        return json_encode(compact('status', 'result'));
    }
    
    public function manager_regist() {
        if($this->request->is('ajax')) {
            $this->autoRender = false;
            $manager = $this->data['Manager'];
            unset($manager['password2']);
            $this->Manager->create();
            $this->Manager->save($manager);
            $status = true;
            $result = null;
            return json_encode(compact('status', 'result'));
        }
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition register-page');
    }    

    public function manager_reg_validation() {
        $this->autoRender = false;
        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
        $cnt = $this->Manager->find('count', array(
            'conditions' => ['account' => $this->data['Manager']['account']],
        ));
        $status = true;
        $result = ['account' => $cnt];
        return json_encode(compact('status', 'result'));
    }
    
    public function manager_index(){
        $this->redirect(['controller'=>'List', 'action'=>'index']);
    }

    public function manager_login(){
        $this->login();
    }

    public function manager_logout(){
        $this->logout();
    }
    
    public function beforeFilter(){
        parent::beforeFilter();
		$this->Auth->allow('login', 'regist', 'reg_validation', 'manager_login', 'manager_regist', 'manager_reg_validation');
	}
}

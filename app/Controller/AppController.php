<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
        'DebugKit.Toolbar',
		'Session', 
        'Cookie',
		'Auth' => array(
			'loginAction' => array('controller' => 'Users','action' => 'login'), //ログインを行なうaction
                       'loginRedirect' => array('controller' => 'Users', 'action' => 'index'), //ログイン後のページ
                       'logoutRedirect' => array('controller' => 'Users', 'action' => 'login'), //ログアウト後のページ
			'authError'=>'ログインして下さい。',
			'authenticate' => array(
            	            'Form' => array(
                            // 認証に利用するモデルの変更
                            'userModel' => 'User',
                            // 認証に利用するモデルのフィードを変更
                            'fields' => array('username' => 'account', 'password' => 'password')
                       )
                )
            )
	);
    public $layout = 'kizukiLayout';
    
    public function beforeFilter(){
        if(isset($this->request->params['manager'])){
                $this->Auth->authenticate = array(
                'Form' => array(
                        'userModel' => 'Manager',
                        'fields' => array('username' => 'account','password'=>'password')
                    )
                );
               $this->Auth->loginAction = array('controller' => 'Users','action' => 'login', 'admin'=>true);
               $this->Auth->loginRedirect = array('controller' => 'Users', 'action' => 'index', 'admin'=>true);
               $this->Auth->logoutRedirect = array('controller' => 'Users', 'action' => 'login', 'admin'=>true);
               AuthComponent::$sessionKey = "Auth.Manager";
            }else{
               AuthComponent::$sessionKey = "Auth.User";
        }
    }
}

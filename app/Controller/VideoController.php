<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP VideoController
 * @author nalu
 */
class VideoController extends AppController {
    public $uses = ['User', 'Manager', 'Movie', 'Kizuki', 'Map'];
    public function index($movieId) {
        $user = $this->Session->read("Auth.User");
        
        $movieData = $this->Movie->find('first', [
            'conditions' => ['id' => $movieId],
            'recursive' => -1
        ]);
        $cnt = $this->Kizuki->find('count', [
            'conditions'=> [
                'user_id'=>$user['id'],
                'movie_id'=>$movieId,
            ]
        ]);
        if ($cnt == 0) {
            $cnt = 1;
        } else {
            $maxCount = $this->Kizuki->find('first', [
                'fields'=>['max(count) as count_max'],
                'conditions'=> [
                        'user_id'=>$user['id'],
                        'movie_id'=>$movieId
                    ]
            ]);
            $cnt = $maxCount[0]['count_max'] + 1;
        }
        
        $this->set('count', $cnt);
        $this->set('movieId', $movieData['Movie']['id']);
        $this->set('movieTitle', $movieData['Movie']['title']);
        $this->set('moviePath', $movieData['Movie']['movie_path']);
        $this->set('title_for_layout',$movieData['Movie']['title'].' - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-yellow layout-top-nav');        
    }

    public function comment() {
        $this->autoRender = false;
        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
        $user = $this->Auth->User();
        $data = $this->request->data;
        $kizukiData = array(
            'movie_id' => $data['movie_id'],
            'user_id' => $user['id'],
            'time' => $data['time'],
            'count' => $data['count'],
            'comment' => $data['comment'],
            'point' => $data['point']
        );
        $this->Kizuki->create();
        $this->Kizuki->save($kizukiData);
    }
    
    public function manager_index($movieId) {
        $movieData = $this->Movie->find('first', array(
            'fields' => array('type', 'movie_path', 'title'),
            'conditions' => array('id' => $movieId),
            'recursive' => -1
        ));
        $this->set('movieId', $movieId);
        $this->set('movieTitle', $movieData['Movie']['title']);
        $this->set('moviePath', $movieData['Movie']['movie_path']);
        $this->set('type', $movieData['Movie']['type']);        
        $this->set('title_for_layout',$movieData['Movie']['title'].' - KIZUKI Project');
        $this->layout = 'kizukiLayout';
        $this->set('class', 'hold-transition skin-green layout-top-nav');                
    }
    
    public function manager_gettimeline() {
        $this->autoRender = false;
        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
        $data = $this->request->data;
        
        
        $this->Kizuki->unbindModel(array(
            'belongsTo' => array('Subject', 'Timetable', 'Movie'),
        ));
        $kizukiData = $this->Kizuki->find('all', array(
            'fields' => array('User.name', 'Kizuki.time', 'Kizuki.count', 'Kizuki.comment', 'Kizuki.point'),
            'conditions' => array('movie_id' => $data['movie_id']),
            'order' => array('time' => 'asc'),
        ));
        $timeline = array();
        for ($i=0; $i<count($kizukiData); $i++) {
            $timeline[] = array(
                $this->_secToStr($kizukiData[$i]['Kizuki']['time']),
                $kizukiData[$i]['User']['name'],
                $kizukiData[$i]['Kizuki']['comment'],
                $kizukiData[$i]['Kizuki']['point'],
                $kizukiData[$i]['Kizuki']['time']
            );
        }
        $status = true;
        $result = $timeline;
        return json_encode(compact('status', 'result'));
    }
    
    public function manager_getlatlag() {
        $this->autoRender = false;
        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
        $data = $this->request->data;
                
        $mapData = $this->Map->find('all', array(
            'conditions' => array('movie_id' => $data['movie_id']),
            'order' => array('time' => 'asc'),
            'recursive' => -1
        ));
        $latlags = array();
        for ($i=0; $i<count($mapData); $i++) {
            $latlags[] = array(
                'time' => $mapData[$i]['Map']['time'],
                'lat' => $mapData[$i]['Map']['lat'],
                'lag' => $mapData[$i]['Map']['lag'],
            );
        }
        $status = true;
        $result = $latlags;
        return json_encode(compact('status', 'result'));
    }
    
    function _secToStr($t) {
        $str = "";
        $m = floor($t / 60);
        if ($m>0) {
            $s = $t - 60;
        } else {
            $s = $t;
        }
        $str = sprintf("00:%02d:", $m);
        if ($s<10) {
            $str.='0'.strval($s);
        } else {
            $str.=strval($s);
        }
        return $str;
    }
}

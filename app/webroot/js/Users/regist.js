/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(function($) {
    $('#regist-btn').click(function() {
        block();
        var $form = $('#UserRegistForm');
        if (!validation($form)) {
            $.unblockUI();
            return false;
        }
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serialize(),
            cache: false,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    showAlert("登録が完了しました。");
                } else {
                    $.unblockUI();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                $.unblockUI();
            }
        });
    });

    $('#info-ok').click(function() {
        $.unblockUI();
        console.log(document.getElementById("login_url").value);
        window.location.href = document.getElementById("login_url").value;
    });

    function validation($form) {
        var vali = true;
        // ニックネーム
        var name = document.getElementById("form-name").value;
        if (name === '') {
            setErrorMessage($form, 'form-name', '※Nicnameを入力してください。');
            vali = false;
        } else {
            clearErrorMessage($form, 'form-name');
        }
        // Account
        var account = document.getElementById("form-account").value;
        if (account === '') {
            setErrorMessage($form, 'form-account', '※Emailを入力してください。');
            vali = false;
        } else {
            clearErrorMessage($form, 'form-account');
        }
        var password = document.getElementById("form-password").value;
        if (password === '') {
            setErrorMessage($form, 'form-password', '※Passwordを入力してください。');
            vali = false;
        } else {
            clearErrorMessage($form, 'form-password');
        }
        var repassword = document.getElementById("form-password2").value;
        if (repassword === '') {
            setErrorMessage($form, 'form-password2', '※Retype passwordを入力してください。');
            vali = false;
        } else {
            clearErrorMessage($form, 'form-password2');
        }
        if (repassword !== '' && password !== '') {
            if (repassword !== password) {
                setErrorMessage($form, 'form-password', '※passwordが一致しません。');
                vali = false;
            } else {
                clearErrorMessage($form, 'form-password');
            }            
        }
        
        if (vali) {
            vali = businessValidation($form);
        }
        return vali;
    }

    function businessValidation($form) {
        var url = document.getElementById("validation_url").value;
        var vali = true;
        $.ajax({
            type: "POST",
            url: url,
            data: $form.serializeArray(),
            dataType: "json",
            async: false,
            success: function(data){
                if (data.result.account > 0) {
                    setErrorMessage($form, 'form-account', '※Emailは登録済みです。');
                    vali = false;
                } else {
                    clearErrorMessage($form, 'form-account');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                vali = false;
            }
        });
        return vali;
    }
    
    function setErrorMessage($form, id, message) {
        $target = $form.find('#'+id);
        $target.parent().attr('form-group has-error');
        $target.parent().find('.text-danger').remove();
        $target.parent().append('<p class="text-danger">'+message+'</p>');
    }
    
    function clearErrorMessage($form, id) {
        $target = $form.find('#'+id);
        $target.parent().attr('form-group has-feedback');
        $target.parent().find('.text-danger').remove();
    }
    
    function showAlert(message) {
        $alert = $(document).find("#info-modal");
        $alert.find('#info-message').text(message);
        $.blockUI({
            message: $('#info-modal'),
            css: {
                width: '275px',
                border: 'none', 
                padding: '10px', 
                backgroundColor: '#000',
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                color: '#fff' 
            },
            theme: false
        });
    }
    
    function block(message) {
        $.blockUI({
            message: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span>'+message+'</span>',
            css: { 
                border: 'none', 
                padding: '10px', 
                backgroundColor: '#000',
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            }
        });         
    }
});
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(function($) {
    // Video
    var glidHnum = 48;
    var glidWnum = 72;
    var glidTbl = [];
    var container = document.getElementById('varea-container');
    var vcanvas = document.getElementById('vcanvas');
    var vctx = vcanvas.getContext('2d');
    var gcanvas = document.getElementById('gcanvas');
    var gctx = gcanvas.getContext('2d');
    var video = document.getElementById("kvideo");
    var k_type = document.getElementById("k_type").value;
    var lastLine = 0;
    var videTimer = null;
    // Plote
    var plot1;
    // map
    var init_lon = 139.752784;                      // 初期表示中心の経度
    var init_lat = 35.684764;                       // 初期表示中心の緯度
    var osmUrl = 'http://192.168.0.10/osm_tiles';    // OSM TILEサーバーのURL
    var rootColor = '#0000FF';                      // ルートの色
    var rootWidth = 4;                      // ルートの線の太さ
    var lineData;
    var latlagData;
    var pointArray = [];
    var olView = new ol.View({
            projection: "EPSG:3857",
            maxZoom: 19,
            zoom: 10
        });
    olView.setZoom(17);                             // 初期表示のZOOM値
    olView.setCenter(ol.proj.transform([parseFloat(init_lon), parseFloat(init_lat)], "EPSG:4326", "EPSG:3857"));
    // Layer: OSM
    var mapLayer = new ol.layer.Tile({
            name : "MapLayer",
            preload: 4,

//            source: new ol.source.OSM({
//                url: osmUrl+"/{z}/{x}/{y}.png",
//                projection: "EPSG:3857",
//                crossOrigin: null
//            }) //openstreet map

            source: new ol.source.OSM()
         });
    var map = new ol.Map({
            target: 'map',
            layers: [
                mapLayer
            ],
            view: olView,
            renderer: ['canvas', 'dom'],
            loadTilesWhileAnimating: true,
            controls: ol.control.defaults(),
            interactions: ol.interaction.defaults()
        });
    map.addControl(new ol.control.ZoomSlider());

    var markerFeature = new ol.Feature({
                  geometry: new ol.geom.Point(ol.proj.transform([parseFloat(init_lon), parseFloat(init_lat)], "EPSG:4326", "EPSG:3857"))
            });
    var markerLayer = new ol.layer.Vector({
                source: new ol.source.Vector({ features: [markerFeature] }),
                style: new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: [0.5, 1],
                        anchorXUnits: 'fraction',
                        anchorYUnits: 'fraction',
                        opacity: 0.75,
                        src: '/images/man.png'          // マーカーの画像
                    })
                })
            });
    

    var rootLayer;
    var crtMap = 0;

    $(function(){
        vcanvas.width = container.clientWidth;
        vcanvas.height = container.clientHeight;
        gcanvas.width = container.clientWidth;
        gcanvas.height = container.clientHeight;
        
        var chat = $('#comment-container');
        chat.height(vcanvas.height);
        
        fillGlidTbl();
        video.oncanplaythrough = function(){
            getTimelie();
            crtDrawVideo();
        };
    });
    
    $(window).resize(function() {
        vcanvas.width = container.clientWidth;
        vcanvas.height = container.clientHeight;
        gcanvas.width = container.clientWidth;
        gcanvas.height = container.clientHeight;
        var chat = $('#comment-container');
        chat.height(vcanvas.height);
        fillGlidTbl();
        if (k_type === 'TL') {
            plot1.replot();
        }
        
        if (video.play)
            crtDrawVideo();
    });

    $('#vplay').on('click', function(){
        if (videTimer === null) {
            $("#comment-container").empty();
            $(this).html('<i class="fa fa-pause"></i>');
            playVideo();
        } else {
            if(video.paused){
                video.play();
                $(this).html('<i class="fa fa-pause"></i>');
            }else{
                video.pause();
                $(this).html('<i class="fa fa-play"></i>');
            }        
        }
    });
    
    $('#vback').on('click', function(){
        $("#comment-container").empty();
        lastLine = 0;
        video.currentTime = 0;
        clearInterval(videTimer);
        videTimer = null;
        if (k_type === 'MP') {
            crtMap = 0;
            moveMap(latlagData[0]['lat'], latlagData[0]['lag']);
        }
        if (!video.play || !video.paused) {
           playVideo();
        }
    });
    
    $('#speed li').on('click', function(){
        var idx = $(this).index();
        var tx = $(this).text();
        $('#speed-btn').html(tx+'<span class="caret"></span>');
        if (idx === 0) {
            video.playbackRate = 2;
        } else if (idx === 1) {
            video.playbackRate = 1;
        } else if (idx === 2) {
            video.playbackRate = 0.5;
        }
    });
    
    function getTimelie() {
        var pdata = {"movie_id": document.getElementById("movie_id").value};
        var url = document.getElementById("get_url").value;
        $.post(url, 
               pdata,
               function (data) {
                    lineData = data.result;
                    if (k_type === 'TL') {
                        drowTimeline();
                    }
                    if (k_type === 'MP') {
                        getLatLag();
                    }
               },
               'json'
        );        
    }
    
    function getLatLag() {
        var pdata = {"movie_id": document.getElementById("movie_id").value};
        var url = document.getElementById("get_latlag_url").value;
        $.post(url, 
               pdata,
               function (data) {
                    latlagData = data.result;
                    initMap(osmUrl, latlagData[0]['lat'], latlagData[0]['lag']);
                    initRoot();
                    initLayer();
               },
               'json'
        );        
    }
    
    function drowTimeline() {

        plot1 = $.jqplot('timeline', [lineData] , {
                        title:'', // グラフタイトル
                        seriesDefaults: {
                            showLine: false,
                            showLabel: false
                            },
                        axes:{
                            // X軸の設定
                            xaxis:{
                                renderer:$.jqplot.DateAxisRenderer,
                                showTicks: true,
                                max: toHms(video.duration),
                                tickOptions: {
                                    formatString:'%M:%S'
                                    }
                                },

                            // Y軸の設定
                            yaxis:{
                                renderer:$.jqplot.CategoryAxisRenderer,
                                label: "",
                                pad: 0,
                                tickOptions: {
                                    showMark:true,
                                    showGridline:true,
                                    formatString:"%s"
                                    }
                                }
                            },
                        
                             highlighter: {
                                show: true,
                                showMarker: true,
                                tooltipLocation: 'n',
                                tooltipAxes: 'xy',
                                formatString: '%s<br />%s'
                             }
        });
        $('#timeline').bind('jqplotDataClick',
            function (ev, seriesIndex, pointIndex, data) {
                var time = plot1.data[seriesIndex][pointIndex][4];
                lastLine = pointIndex;
                clickRect = -1;
                $("#comment-container").empty();
                video.currentTime = time;
                clearInterval(videTimer);
                videTimer = null;
                playVideo();
            });
        
    }
    
    function secToStr(t) {
        var str = "";
        if (t > 60) {
            var m = t / 60;
            var s = t % 60;
            str = String(Math.floor(m))+'分 '+String(s.toFixed(1))+'秒';
        } else {
            str = String(t.toFixed(1))+'秒';
        }
        return str;
    }
    
    function toHms(t) {
            var hms = "";
            var h = t / 3600 | 0;
            var m = t % 3600 / 60 | 0;
            var s = t % 60;

            hms = padZero(h) + ":" + padZero(m) + ":" + padZero(s);
    
            return hms;

            function padZero(v) {
                    if (v < 10) {
                            return "0" + v;
                    } else {
                            return v;
                    }
            }
    }    

    function fillGlidTbl() {
        var w = vcanvas.width / glidWnum;
        var h = vcanvas.height / glidHnum;

        glidTbl = [];
        for (var i=0; i<vcanvas.height / h; i++) {
            for (var j=0; j< vcanvas.width / w; j++) {
                glidTbl.push([ Math.floor(j*w), Math.floor(i*h)]);
            }
        }
    }
    
    function setClickPoint(num) {
        pointArray.push({
           'num': num,
           'radius': vcanvas.width / glidWnum * 2, 
           'alpha': 1.0,
           'lasttime': 0
        });
        
    }
    
    function drawClikpoint() {
        var w = vcanvas.width / glidWnum;
        var h = vcanvas.height / glidHnum;
        var date = new Date();
        var crtTime = date.getTime();
        vctx.fillStyle = 'rgb(255, 50, 50)';
        for (var i=0; i<pointArray.length; i++) {
            vctx.globalAlpha = pointArray[i]['alpha'];
            vctx.arc(glidTbl[pointArray[i]['num']][0]+w/2, glidTbl[pointArray[i]['num']][1]+h/2, pointArray[i]['radius'], 0, Math.PI*2, true);
            vctx.fill();
            vctx.globalAlpha = 1.0;
            vctx.beginPath();
            if (!video.paused) {
                if (pointArray[i]['lasttime'] + 60 < crtTime) {
                   pointArray[i]['alpha']-=0.03;
                   pointArray[i]['radius']-=2.0;
                   pointArray[i]['lasttime'] = crtTime;
               }
            } else {
               pointArray[i]['lasttime'] = crtTime;
            }
            if (pointArray[i]['radius']<=1) {
                pointArray.splice(i, 1);
            }
        }
    }
    
    function drowComment() {
        var crt_time = video.currentTime;
        var w = vcanvas.width / glidWnum;
        for (var i=lastLine; i < lineData.length; i++) {
            if (lineData[i][4] <= crt_time) {
                setClickPoint(lineData[i][3]);
                var line_base = create_timeline(lineData[i][1], lineData[i][2], parseFloat(lineData[i][4]));
                $("#comment-container").append(line_base);
                lastLine = i+1;
                $('#comment-container').animate({scrollTop: $('#comment-container')[0].scrollHeight}, 'fast');
            }
        }
    }
    
    function setMap() {
        if (k_type === 'MP') {
            if (crtMap+1 >= latlagData.length) {
                return;
            }
            var crt_time = video.currentTime;
            if (latlagData[crtMap+1]['time'] <= crt_time) {
                crtMap++;
                moveMap(latlagData[crtMap]['lat'], latlagData[crtMap]['lag']);
            }
        }
    }
    
    function create_timeline(u, c, t) {
        var line_base = $("#line_base").clone(true);
        line_base.find("#line_u").empty();
        line_base.find("#line_u").append(u);
        line_base.find("#line_c").empty();
        line_base.find("#line_c").append(c);
        line_base.find("#line_t").empty();
        line_base.find("#line_t").append(secToStr(t));
        line_base.show();
        return line_base;
    }
    
    
    function playVideo() {
        video.play();
        videTimer = setInterval(function(){
            vcanvas.getContext("2d").drawImage(video, 0, 0, container.clientWidth, container.clientHeight);
            if (video.ended) {
                document.getElementById("vplay").innerHTML = '<i class="fa fa-play"></i>';
                clearInterval(videTimer);
                videTimer = null;
            }
            drowComment();
            drawClikpoint();
            setMap();
        }, 1000/60);
    }
    
    function crtDrawVideo() {
        vcanvas.getContext("2d").drawImage(video, 0, 0, container.clientWidth, container.clientHeight);
    }

    /*
     * Map
     */
    function moveMap(lat, lag) {
        olView.setCenter(ol.proj.transform([parseFloat(lag), parseFloat(lat)], "EPSG:4326", "EPSG:3857"));
        markerFeature.setGeometry(new ol.geom.Point(ol.proj.transform([parseFloat(lag), parseFloat(lat)], "EPSG:4326", "EPSG:3857")));

    }

    function initRoot() {
        var root = [];
        for (var i=0; i<latlagData.length; i++) {
            root[i] = [parseFloat(latlagData[i]['lag']), parseFloat(latlagData[i]['lat'])];
        }
        var lineArray = [];
        for (i=0; i<(root.length-1); i++) {
             lineArray.push([root[i], root[i+1]]);
        }
        var lineStrings = new ol.geom.MultiLineString([]);
        lineStrings.setCoordinates(lineArray);
        var vectorFeature = new ol.Feature(
            lineStrings.transform('EPSG:4326', 'EPSG:3857')
        );

        var vectorSource  = new ol.source.Vector({
            features: [vectorFeature]
        });
        // 経路用の vector layer の作成
        rootLayer = new ol.layer.Vector({
            source: vectorSource,
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({ color: rootColor, width: rootWidth })
            })
        });
    }
    
    function initLayer() {
        // root layer の追加
        map.addLayer(rootLayer);
        // Marker layer の追加
        map.addLayer(markerLayer);
    }
    function initMap(url, lat, lon) {
        olView.setCenter(ol.proj.transform([parseFloat(lon), parseFloat(lat)], "EPSG:4326", "EPSG:3857"));
        markerFeature.setGeometry(new ol.geom.Point(ol.proj.transform([parseFloat(lon), parseFloat(lat)], "EPSG:4326", "EPSG:3857")));
        
//        rootLayer.setVisible(false);
    }
});

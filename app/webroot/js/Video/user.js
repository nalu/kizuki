/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(function($) {
    var glidHnum = 48;
    var glidWnum = 72;
    var glidTbl = new Array();
    var clickRect = -1;
    var radius = 0;
    var alpha = 0;
    var videTimer;
    var ctime = 0;
    var lastTime;
    var vpause = false;
    
    var tmpTime = 0;
    var container = document.getElementById('varea-container');
    var vcanvas = document.getElementById('vcanvas');
    var vctx = document.getElementById('vcanvas').getContext('2d');
    
    var v = document.getElementById("kvideo");
    vcanvas.width = container.clientWidth;
    vcanvas.height = container.clientHeight;
    v.load();
    v.onloadeddata = function(){
        drawStart();
        vcanvas.addEventListener('click', onStart, false);
        drawVideTime();
        v.onloadeddata = function(){}
    }
    
    $(window).resize(function() {
        vcanvas.width = container.clientWidth;
        vcanvas.height = container.clientHeight;
    });
    
    $('#videoend').dialog({
        autoOpen: false,
        title: 'KIZUKI',
        closeOnEscape: false,
        modal: true,
        buttons: {
            "OK": function(){
                clearInterval(videTimer);
                var url = document.getElementById("return_url").value;
                window.location.href = url;
            }
        }
    });
    
    $('#addtime').dialog({
        autoOpen: false,
        title: 'KIZUKI',
        closeOnEscape: false,
        modal: true,
        buttons: {
            "CANCEL": function() {
                $(this).dialog('close');
                if (browser() == 'chrome') {
                    var v = document.getElementById("kvideo");
                    v.play();
                } else {
                    vpause = false;
                }
            },
            "　OK　": function(){
                var c = $("#comment").val();
                if (!c) {
                    c = "コメントなし";
                }
                var v = document.getElementById("kvideo");
                var t = v.currentTime;
                var line_base = createTimeline(c, t);
                $("#ktimeline").append(line_base);
                sendComment(c, t);
                $(this).dialog('close');
                $("#comment").val('');
                if (browser() == 'chrome') {
                    v.play();
                } else {
                    vpause = false;
                }
            }
        }
    });

    function sendComment(c, t) {
        if (clickRect < 0)
            return;
        var pdata = {"movie_id": document.getElementById("movie_id").value,
                     "count": document.getElementById("crt_count").value,
                     "point": clickRect,
                     "comment": c,
                     "time": t
                 };
        var url = document.getElementById("send_url").value;
        $.post(url, pdata);
    }
    
    function createTimeline(c, t) {
        var line_base = $("#line_base").clone(true);
        line_base.find("#line_c").empty();
        line_base.find("#line_c").append(c);
        line_base.find("#line_t").empty();
        line_base.find("#line_t").append(secToStr(t));
        line_base.show();
        return line_base;
    }
    
    function secToStr(t) {
        var str = "";
        if (t > 60) {
            var m = t / 60;
            var s = t % 60;
            str = String(Math.floor(m))+'分 '+String(s.toFixed(1))+'秒'
        } else {
            str = String(t.toFixed(1))+'秒'
        }
        return str;
    }
    
    function fillGlidTbl() {
        var w = vcanvas.width / glidWnum;
        var h = vcanvas.height / glidHnum;

        glidTbl = [];
        for (var i=0; i<vcanvas.height / h; i++) {
            for (var j=0; j< vcanvas.width / w; j++) {
                glidTbl.push([ Math.floor(j*w), Math.floor(i*h)]);
            }
        }
    }
    
    function getClickArea(x, y) {
        var w = vcanvas.width / glidWnum;
        var h = vcanvas.height / glidHnum;
        for (var i=0; i<glidTbl.length; i++) {
            if (inRect(glidTbl[i], w, h, x, y)) {
                radius=w*1.5;
                alpha = 1.0;
                return i;
            }
        }
        return -1;
    }
    
    function inRect(bp, w, h, x, y) {
        if ( bp[1] > y || bp[1]+h < y || bp[0] > x || bp[0]+w < x ) {
            return false;
        } else {
            return true;
        }        
    }
    
    function drawStart() {
        var img = new Image();    //画像オブジェクト作成
        crtDrawVideo();
        img.src = "/movie/play-button.png"; 
        img.onload = function() {
            var x = (vcanvas.width - img.width)/2
            var y = (vcanvas.height - img.height)/2;
            vcanvas.getContext("2d").drawImage(img, x, y);
        };
    }
    
    function drawClikpoint() {
        if (clickRect > -1) {
            var w = vcanvas.width / glidWnum;
            var h = vcanvas.height / glidHnum;
            vctx.beginPath();
            vctx.fillStyle = 'rgb(255, 50, 50)';
            vctx.globalAlpha = alpha;
            vctx.arc(glidTbl[clickRect][0]+w/2, glidTbl[clickRect][1]+h/2, radius, 0, Math.PI*2, true);
            vctx.fill();
            if (browser() == 'chrome') {
                var v = document.getElementById("kvideo");
                if (!v.paused) {
                    radius-=1;
                    alpha-=0.05;
                    if (radius<0) {
                        clickRect = -1;
                    }
                }
            } else {
                radius-=1;
                alpha-=0.05;
                if (radius<0) {
                    clickRect = -1;
                }                
            }
        }
    }
    
    function onStart(e) {
        vcanvas.addEventListener('click', onGClick, false);
        playVideo();
    }
    
    function onGClick(e) {
        var rect = e.target.getBoundingClientRect();
        var x = e.clientX - rect.left;
        var y = e.clientY - rect.top;
        clickRect = getClickArea(x, y);
        if (browser() == 'chrome') {
            var v = document.getElementById("kvideo");
            v.pause();
        } else {
            vpause = true;
        }
        drawClikpoint();
        $('#addtime').dialog('open');
    }

    function playVideo() {
        var video = document.getElementById("kvideo");
        lastTime = Date.now();
        if (browser() == 'chrome') {
            video.play();
            videTimer = setInterval(function(){
                    vcanvas.getContext("2d").drawImage(video, 0, 0, container.clientWidth, container.clientHeight);
                    fillGlidTbl();
                    drawClikpoint();
                    if (video.ended) {
                        clickRect = -1;
                        $('#videoend').dialog('open');
                    } else {
                        drawVideTime();
                    }
            }, 1000/60);
        } else {
            videTimer = setInterval(function(){
                if (!vpause) {
                    var curTime = Date.now();
                    var diff = Date.now() - lastTime;
                    lastTime = curTime;
                    ctime += diff/1000;
                    video.currentTime = ctime;
                    vcanvas.getContext("2d").drawImage(video, 0, 0, container.clientWidth, container.clientHeight);
                    fillGlidTbl();
                    drawClikpoint();
                    if(video.duration <= video.currentTime){
                        clickRect = -1;
                        $('#videoend').dialog('open');
                    } else {
                        drawVideTime();
                    }
                 } else {
                    lastTime = Date.now();
                }
            }, 1000/60);
        }
    }
    
    function browser() {
        var userAgent = window.navigator.userAgent.toLowerCase();

        // ブラウザ判定
        if (userAgent.indexOf('msie') != -1) {
          /* IE. */
          return 'ie';
        } else if (userAgent.indexOf('chrome') != -1) {
          /* Google Chrome. */
          return 'chrome';
        } else if (userAgent.indexOf('firefox') != -1) {
          /* FireFox. */
          return 'firefox';
        } else if (userAgent.indexOf('safari') != -1) {
          /* Safari. */
          return 'safari';
        } else if (userAgent.indexOf('opera') != -1) {
          /* Opera. */
          return 'opera';
        } else if (userAgent.indexOf('gecko') != -1) {
          /* Gecko. */
          return 'gecko';
        } else {
          return false;
        }
    }
    
    function crtDrawVideo() {
        var video = document.getElementById("kvideo");
        vcanvas.getContext("2d").drawImage(video, 0, 0, container.clientWidth, container.clientHeight);
    }
    
    function drawVideTime() {
        var v = document.getElementById("kvideo");
        var duration = Math.floor(v.duration * 100) / 100;
        var currentTime = Math.floor(v.currentTime * 100) / 100;
        $('#movie-time').text(currentTime + "/" + duration + " Sec.");
    }
});
    

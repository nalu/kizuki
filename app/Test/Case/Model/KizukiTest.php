<?php
App::uses('Kizuki', 'Model');

/**
 * Kizuki Test Case
 */
class KizukiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.kizuki',
		'app.movie',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Kizuki = ClassRegistry::init('Kizuki');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Kizuki);

		parent::tearDown();
	}

}

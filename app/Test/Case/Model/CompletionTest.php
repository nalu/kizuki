<?php
App::uses('Completion', 'Model');

/**
 * Completion Test Case
 */
class CompletionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.completion',
		'app.student'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Completion = ClassRegistry::init('Completion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Completion);

		parent::tearDown();
	}

}

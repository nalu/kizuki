<!DOCTYPE html>
<html lang="ja">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="noindex,nofollow">
    
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
   <?php

        echo $this->Html->meta('icon');
        // CSS
        echo $this->Html->css('//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css');
        // Bootstrap
    	echo $this->Html->css('bootstrap.min');
        // Font Awesome
    	echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
        // Ionicons
    	echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
        // Theme style
        echo $this->Html->css('AdminLTE.min');
        echo $this->Html->css('skins/_all-skins.min');
        // iCheck
        echo $this->Html->css('iCheck/square/blue.css');
        // jqplot
        echo $this->Html->css('jquery.jqplot.min.css');
        
        echo $this->fetch('meta');
        echo $this->fetch('css');
    ?>
    <style type="text/css">
    <!--
       .row-eq-height {
        display: flex;
        flex-wrap: wrap;
    }-->
    </style>

</head>
<body class="<?php echo $class ?>">
<?php echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>
<?php
        // js
        // jQuery
        echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
        // jQuery UI
        echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
        // Bootstrap
        echo $this->Html->script('bootstrap.min');
        // iCheck
        echo $this->Html->script('iCheck/icheck.min');
        // SlimScroll
        echo $this->Html->script('jquery.slimscroll.min');
        // FastClick -->
        echo $this->Html->script('fastclick.min');
        // AdminLTE App -->
        echo $this->Html->script('app.min');
        // AdminLTE for demo purposes -->
        echo $this->Html->script('demo');
        // kizuki project js
        echo $this->fetch('script');
?>
</body>
</html>

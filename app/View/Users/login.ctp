<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>KIZUKI</b> </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <?php echo $this->Session->flash(); ?>

    <?php
        echo $this->Form->create('User', array(
                'type' => 'post',
                'url' => array("controller"=>"Users", "action"=>"login"),
              ));
    ?>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('account', array(
                        'type' => 'email',
                        'id' => 'form-account',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Email',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('password', array(
                        'id' => 'form-password',
                        'type' => 'password',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    <?php
        echo $this->Form->end();
    ?>

    <a href="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"regist")); ?>" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

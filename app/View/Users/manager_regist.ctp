<?php
    echo $this->Html->script('Users/manager_regist.js', array('inline' => false));
    echo $this->Html->script('jquery.blockUI.js', array('inline' => false));
?>

<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>KIZUKI</b> manager</a>
  </div>
  <div class="register-box-body">
    <input type="hidden" id="validation_url" value="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"manager_reg_validation")); ?>">
    <input type="hidden" id="login_url" value="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"manager_login")); ?>">
    <p class="login-box-msg">Register a new membership</p>

    <?php
        echo $this->Form->create('Manager', array(
                'type' => 'post',
                'url' => array("controller"=>"Users", "action"=>"regist"),
              ));
    ?>        
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('name', array(
                        'id' => 'form-name',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Nickname',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('account', array(
                        'id' => 'form-account',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Account',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('password', array(
                        'id' => 'form-password',
                        'type' => 'password',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('password2', array(
                        'id' => 'form-password2',
                        'type' => 'password',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Retype password',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="button" class="btn btn-primary btn-block btn-flat" id="regist-btn">Register</button>
        </div>
        <!-- /.col -->
      </div>
    <?php
        echo $this->Form->end();
    ?>

    <a href="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"login")); ?>" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->


<div id="info-modal" style="display:none; cursor: default">
    <span class="glyphicon glyphicon-ok" id="info-message"></span>
    <p id="info-message"></p>
    <input type="button" class="btn btn-outline" id="info-ok" value="OK" /> 
</div>

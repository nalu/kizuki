<?php
    echo $this->Html->script('Users/regist.js', array('inline' => false));
    echo $this->Html->script('jquery.blockUI.js', array('inline' => false));
?>
<div class="register-box">
  <div class="register-logo">
    <a href="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"login")); ?>"><b>KIZUKI</b></a>
  </div>

  <div class="register-box-body">
    <input type="hidden" id="validation_url" value="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"reg_validation")); ?>">
    <input type="hidden" id="login_url" value="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"login")); ?>">
    <p class="login-box-msg">Register a new membership</p>
    <?php
        echo $this->Form->create('User', array(
                'type' => 'post',
                'url' => array("controller"=>"Users", "action"=>"regist"),
              ));
    ?>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('name', array(
                        'id' => 'form-name',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Nickname',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('account', array(
                        'type' => 'email',
                        'id' => 'form-account',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Email',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('password', array(
                        'id' => 'form-password',
                        'type' => 'password',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('password2', array(
                        'id' => 'form-password2',
                        'type' => 'password',
                        'label' => false,
                        'div' => false,
                        'class' => 'form-control',
                        'placeholder' => 'Retype password',
                    ));
        ?>                            
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" class="btn btn-primary btn-block btn-flat" id="regist-btn">Register</button>
        </div>
        <!-- /.col -->
      </div>
    <?php
        echo $this->Form->end();
    ?>
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>

    <a href="<?php echo $this->html->url(array("controller"=>"Users", "action"=>"login")); ?>" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<div id="info-modal" style="display:none; cursor: default">
    <span class="glyphicon glyphicon-ok" id="info-message"></span>
    <p id="info-message"></p>
    <input type="button" class="btn btn-outline" id="info-ok" value="OK" /> 
</div>

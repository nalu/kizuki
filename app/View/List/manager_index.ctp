<?php
    echo $this->Html->script('vlist.js', array('inline' => false));
?>
<div class="wrapper">

<?php echo $this->element('mheader'); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">

      <!-- Main content -->
    <section class="content">



      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <?php for ($i=0; $i<count($movieList); $i++) { ?>  
                <tr data-href="<?php echo $this->html->url(array("controller"=>"Video", "action"=>"index", $movieList[$i]['id'])) ?>">
                  <td><b><?php echo $movieList[$i]['title']; ?></b></td>
                  <td>
                      <?php if ($movieList[$i]['type'] == 'TL') { ?>
                          <span class="label label-info">Tome line</span>
                      <?php } elseif ($movieList[$i]['type'] == 'MP') { ?>
                          <span class="label label-success">Map</span>
                      <?php } ?>
                  </td>
                  <td><i><?php echo $movieList[$i]['created']; ?> registered</i></td>
                  <td><span class="label label-danger"><?php echo $movieList[$i]['kizukiCount']; ?> kizuki</span></td>
                  <td><a href="<?php echo $this->html->url(["controller"=>"List", "action"=>"edit", $movieList[$i]['id']]) ?>" class="pull-right"><i class="glyphicon glyphicon-pencil"></i></a></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
              <div class="box-footer">
                  <a href="<?php echo $this->html->url(["controller"=>"List", "action"=>"add"]) ?>" class="btn btn-default pull-right"><i class="glyphicon glyphicon-plus"></i>追加</a>
              </div>
          </div>
          <!-- /.box -->
        </div>
      </div>





      </section>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php echo $this->element('footer'); ?>
</div>
<!-- ./wrapper -->


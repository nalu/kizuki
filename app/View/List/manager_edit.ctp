<div class="wrapper">

<?php echo $this->element('mheader'); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">

      <!-- Main content -->
    <section class="content">



      <!-- /.row -->
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="box box-danger">
            <!-- /.box-header -->
            <div class="box-header with-border">
              <h3 class="box-title">動画編集</h3>
            </div>
            <?php echo $this->Form->create('Movie', ['url' => 'edit', 'enctype' => 'multipart/form-data']); ?>
            <?php echo $this->Form->hidden('id', ['value' => $this->data['Movie']['id']])?>
            <div class="box-body">
                <div class="form-group">
                  <label for="form-movie-path">動画ファイルのパス</label>
                <?php
                    echo $this->Form->input('movie_path', [
                                'label' => false,
                                'id' => 'form-movie-path',
                                'escape' =>false,
                                'class' => 'form-control',
                                'div' => false
                            ]
                        );
                ?>
                </div>
                <div class="form-group">
                  <label for="form-title">タイトル</label>
                <?php
                    echo $this->Form->input('title', [
                                'label' => false,
                                'id' => 'form-title',
                                'escape' =>false,
                                'class' => 'form-control',
                                'div' => false
                            ]
                        );
                ?>
                </div>
                <div class="form-group">
                  <p class="control-label"><b>タイプ</b></p>
                <?php
                echo $this->Form->radio('type', $types, [
                                'id' => 'form-type',
                                'legend' => false,
                                'div' => false
                            ]
                        );
                ?>
                </div>
                <div class="form-group">
                  <label for="form-csvfile">ロケーションCSV</label>
                    <?php
                        echo $this->Form->input('csvfile', [
                                    'type' => 'file',
                                    'label' => false,
                                    'id' => 'form-csvfile',
                                    'escape' =>false,
                                    'class' => false,
                                    'div' => false
                                ]
                            );
                    ?>
                </div>
            </div>                
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="<?php echo $this->html->url(["controller"=>"List", "action"=>"index"]) ?>" class="btn btn-danger">CANCEL</a>
              <button type="submit" class="btn btn-primary pull-right" id="add-btn">　OK　</button>
              <a href="<?php echo $this->html->url(["controller"=>"List", "action"=>"delete", $this->data['Movie']['id']]) ?>" class="btn btn-warning pull-right">DELETE</a>
            </div>
            <?php echo $this->Form->end(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>





      </section>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php echo $this->element('footer'); ?>
</div>
<!-- ./wrapper -->



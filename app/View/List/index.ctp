<?php
    echo $this->Html->script('vlist.js', array('inline' => false));
?>
<div class="wrapper">

<?php echo $this->element('header'); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Video list
        <small></small>
      </h1>
    </section>

      <!-- Main content -->
      <section class="content">



      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <?php for ($i=0; $i<count($movieList); $i++) { ?>  
                <tr data-href="<?php echo $this->html->url(array("controller"=>"Video", "action"=>"index", $movieList[$i]['id'])) ?>">
                    <td><b><?php echo $movieList[$i]['title']; ?></b></td>
                      <td><span class="label label-success"><?php echo $movieList[$i]['kizukiCount']; ?>回</span></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>





      </section>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php echo $this->element('footer'); ?>
</div>
<!-- ./wrapper -->



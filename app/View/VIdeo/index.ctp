<?php
    echo $this->Html->script('Video/user.js', array('inline' => false));
?>

<div class="wrapper">

<?php echo $this->element('header'); ?>

    <!-- Full Width Column -->
  <input type="hidden" id="movie_id" value="<?php echo $movieId; ?>">
  <input type="hidden" id="crt_count" value="<?php echo $count; ?>">
  <input type="hidden" id="return_url" value="<?php echo $this->html->url(array("controller"=>"List", "action"=>"index")) ?>">
  <input type="hidden" id="send_url" value="<?php echo $this->html->url(array("controller"=>"Video", "action"=>"comment")); ?>">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $movieTitle; ?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->html->url(array("controller"=>"List", "action"=>"index")) ?>"><i class="fa fa-dashboard"></i> Video list</a></li>
        <li class="active"><?php echo $movieTitle; ?></li>
      </ol>
    </section>
      <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-6">
          <div class="box box-warning">
            <div class="box-body">
                <div class="embed-responsive  embed-responsive-16by9" id="varea-container">
                <video id="kvideo" style="display:none;">
                    <source src="<?php echo $moviePath; ?>">
                </video>
                    <canvas id="vcanvas" style="z-index: 1; "></canvas>
                </div>
                <p class="text-right" id="movie-time"></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <!-- DIRECT CHAT PRIMARY -->
          <div class="box box-warning direct-chat direct-chat-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Kizuki point</h3>
              <div class="box-tools pull-right">
                <span data-toggle="tooltip" title="<?php echo $count; ?> Times" class="badge bg-green"><?php echo $count; ?></span>
              </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body" id="ktimeline">
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.col -->
          
      </div><!-- row -->






      </section>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php echo $this->element('footer'); ?>
</div>
<!-- ./wrapper -->

<div id="addtime"> 
    <div class="form-group has-success">
      <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> コメント</label>
      <input type="text" class="form-control" id="comment">
    </div>
</div>
<div id="videoend"> 
    <div class="form-group has-success">
        <p>再生が終了しました。</p>
    </div>
</div>
<div class="direct-chat-msg" id="line_base" style="display:none">
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-timestamp pull-right" id="line_t"></span>
  </div>
  <!-- /.direct-chat-info -->
  <img class="direct-chat-img" src="<?php echo $this->Html->url( "/" )?>img/sample/sample-user.png" alt="Message User Image">
  <div class="direct-chat-text" id="line_c">
    
  </div>
  <!-- /.direct-chat-text -->
</div>

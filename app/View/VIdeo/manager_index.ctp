<?php
    echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/openlayers/4.3.1/ol.css', ['inline' => false]);
    echo $this->Html->script('jquery.jqplot.min.js', array('inline' => false));
    echo $this->Html->script('plugins/jqplot.cursor.js', array('inline' => false));
    echo $this->Html->script('plugins/jqplot.dateAxisRenderer.js', array('inline' => false));
    echo $this->Html->script('plugins/jqplot.categoryAxisRenderer.js', array('inline' => false));
    echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/openlayers/4.3.1/ol.js', array('inline' => false));
    echo $this->Html->script('Video/manager.js', array('inline' => false));
?>

<div class="wrapper">

<?php echo $this->element('mheader'); ?>
    
  <input type="hidden" id="movie_id" value="<?php echo $movieId; ?>">
  <input type="hidden" id="get_url" value="<?php echo $this->html->url(array("controller"=>"Video", "action"=>"gettimeline")); ?>">
  <input type="hidden" id="get_latlag_url" value="<?php echo $this->html->url(array("controller"=>"Video", "action"=>"getlatlag")); ?>">
  <input type="hidden" id="k_type" value="<?php echo $type; ?>">
    
    
    
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $movieTitle; ?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->html->url(array("controller"=>"List", "action"=>"index")) ?>"><i class="fa fa-dashboard"></i> Video List</a></li>
        <li class="active"><?php echo $movieTitle; ?></li>
      </ol>
    </section>
      <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-default">
              <div class="box-body">
                  <div class="embed-responsive  embed-responsive-16by9" id="varea-container">
                      <video id="kvideo" src="<?php echo $moviePath; ?>" preload="auto" style="display:none;"></video>
                      <canvas id="vcanvas" style="z-index: 1; "></canvas>
                      <canvas id="gcanvas" style="z-index: 100; background-color: transparent;"></canvas>
                  </div>
                  <div class="btn-group">
<!--                  <button type="button" id="vplay" class="btn btn-default btn-block"><i class="fa fa-play"></i></button> -->
                  <button type="button" id="vback" class="btn btn-default"><i class="fa fa-fast-backward"></i></button>
                  <button type="button" id="vplay" class="btn btn-default"><i class="fa fa-play"></i></button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="speed-btn">
                        x1
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" id="speed">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">x2</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">x1</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">x0.5</a></li>
                    </ul>
                </div>

                  </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        <!-- /.col -->
        
        <div class="col-md-6">
          <!-- DIRECT CHAT PRIMARY -->
          <div class="box box-default direct-chat direct-chat-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-comments"></i>COMMENT</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="embed-responsive direct-chat-messages" id="comment-container">
                    
                </div>                
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box-footer-->
        </div>
          <!--/.direct-chat -->
      </div>
        <!-- /.col -->
        
      <div class="row">
      <?php if ($type == 'TL') {?>
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-clock-o"></i> TIME LINE</h3>
        </div>
        <div class="box-body">
                <div id="timeline"></div>
        </div>
        </div>
      <?php } elseif ($type == 'MP') { ?>
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-map"></i> Map</h3>
        </div>
        <div class="box-body">
            <div id="map" style="width:100%; height:350px"></div>
        </div>
        </div>
      <?php } elseif ($type == 'OSM') { ?>
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-map"></i> Map</h3>
        </div>
        <div class="box-body">
            <div id="map" style="width:100%; height:350px"></div>
        </div>
        </div>
      <?php } ?>
      </div>
      
    </section>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php echo $this->element('footer'); ?>

<!-- ./wrapper -->

<div class="direct-chat-msg" id="line_base" style="display:none">
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-name pull-left" id="line_u"></span>
    <span class="direct-chat-timestamp pull-right" id="line_t"></span>
  </div>
  <!-- /.direct-chat-info -->
  <img class="direct-chat-img" src="<?php echo $this->Html->url( "/" )?>img/sample/sample-user.png" alt="Message User Image">
  <div class="direct-chat-text" id="line_c">
    
  </div>
  <!-- /.direct-chat-text -->
</div>
 <span id="info"></span>

<?php
App::uses('AppModel', 'Model');
/**
 * Movie Model
 *
 * @property Kizuki $Kizuki
 * @property Map $Map
 */
class Movie extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Kizuki' => array(
			'className' => 'Kizuki',
			'foreignKey' => 'movie_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Map' => array(
			'className' => 'Map',
			'foreignKey' => 'movie_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

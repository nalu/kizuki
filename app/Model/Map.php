<?php
App::uses('AppModel', 'Model');
/**
 * Map Model
 *
 * @property Movie $Movie
 */
class Map extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Movie' => array(
			'className' => 'Movie',
			'foreignKey' => 'movie_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function loadCSV($movie_id, $filename){
        $datasource = $this->getDataSource();
        $datasource->begin();
        try{
            $this->deleteAll('movie_id='.$movie_id, false);
            $csvData = file($filename, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            foreach($csvData as $line) {
                $line = $this->deleteBom($line);
                $record = explode(',', $line);
                if (count($record) !== 3) {
                    $this->log('Bad column number');
                    throw new Exception();
                }
                $data = [
                    'movie_id' => $movie_id,
                    'time' => $record[0] == ''? NULL: $record[0],
                    'lat' => $record[1] == ''? NULL: $record[1],
                    'lag' => $record[2] == ''? NULL: $record[2],
                ];
                $this->create($data);
                $this->save();
            }
            $datasource->commit();
        }catch(Exception $e) {
            $datasource->rollback();
            throw new InternalErrorException();
        }
    }
    
    function deleteBom($str) {
        if (($str == NULL) || (mb_strlen($str) == 0)) {
            return $str;
        }
        if (ord($str{0}) == 0xef && ord($str{1}) == 0xbb && ord($str{2}) == 0xbf) {
            $str = substr($str, 3);
        }
        return $str;
    }
}

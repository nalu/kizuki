<?php
App::uses('AppModel', 'Model');
/**
 * Manager Model
 *
 */
class Manager extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
}
